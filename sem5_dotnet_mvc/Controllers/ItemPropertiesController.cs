using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sem5_dotnet_mvc.Data;
using sem5_dotnet_mvc.Models;

namespace sem5_dotnet_mvc.Controllers
{
    public class ItemPropertiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ItemPropertiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ItemProperties
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ItemProperties.Include(i => i.Item).Include(i => i.Property);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: ItemProperties/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null || _context.ItemProperties == null)
            {
                return NotFound();
            }

            var itemProperty = await _context.ItemProperties
                .Include(i => i.Item)
                .Include(i => i.Property)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemProperty == null)
            {
                return NotFound();
            }

            return View(itemProperty);
        }

        // GET: ItemProperties/Create
        public IActionResult Create()
        {
            ViewData["Itemid"] = new SelectList(_context.Items, "Id", "Id");
            ViewData["PropertyId"] = new SelectList(_context.Properties, "Id", "Id");
            return View();
        }

        // POST: ItemProperties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,PropertyId,Itemid")] ItemProperty itemProperty)
        {
            if (ModelState.IsValid)
            {
                _context.Add(itemProperty);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Itemid"] = new SelectList(_context.Items, "Id", "Id", itemProperty.Itemid);
            ViewData["PropertyId"] = new SelectList(_context.Properties, "Id", "Id", itemProperty.PropertyId);
            return View(itemProperty);
        }

        // GET: ItemProperties/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null || _context.ItemProperties == null)
            {
                return NotFound();
            }

            var itemProperty = await _context.ItemProperties.FindAsync(id);
            if (itemProperty == null)
            {
                return NotFound();
            }
            ViewData["Itemid"] = new SelectList(_context.Items, "Id", "Id", itemProperty.Itemid);
            ViewData["PropertyId"] = new SelectList(_context.Properties, "Id", "Id", itemProperty.PropertyId);
            return View(itemProperty);
        }

        // POST: ItemProperties/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,PropertyId,Itemid")] ItemProperty itemProperty)
        {
            if (id != itemProperty.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(itemProperty);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemPropertyExists(itemProperty.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Itemid"] = new SelectList(_context.Items, "Id", "Id", itemProperty.Itemid);
            ViewData["PropertyId"] = new SelectList(_context.Properties, "Id", "Id", itemProperty.PropertyId);
            return View(itemProperty);
        }

        // GET: ItemProperties/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null || _context.ItemProperties == null)
            {
                return NotFound();
            }

            var itemProperty = await _context.ItemProperties
                .Include(i => i.Item)
                .Include(i => i.Property)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemProperty == null)
            {
                return NotFound();
            }

            return View(itemProperty);
        }

        // POST: ItemProperties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            if (_context.ItemProperties == null)
            {
                return Problem("Entity set 'ApplicationDbContext.ItemProperties'  is null.");
            }
            var itemProperty = await _context.ItemProperties.FindAsync(id);
            if (itemProperty != null)
            {
                _context.ItemProperties.Remove(itemProperty);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemPropertyExists(string id)
        {
          return (_context.ItemProperties?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
