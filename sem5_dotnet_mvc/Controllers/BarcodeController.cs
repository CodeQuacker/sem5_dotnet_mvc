using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sem5_dotnet_mvc.Data;
using sem5_dotnet_mvc.Models;

namespace sem5_dotnet_mvc.Controllers
{
    public class BarcodeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BarcodeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Barcode
        public async Task<IActionResult> Index()
        {
              return _context.Barcodes != null ? 
                          View(await _context.Barcodes.ToListAsync()) :
                          Problem("Entity set 'ApplicationDbContext.Barcodes'  is null.");
        }

        // GET: Barcode/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null || _context.Barcodes == null)
            {
                return NotFound();
            }

            var barcode = await _context.Barcodes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (barcode == null)
            {
                return NotFound();
            }

            return View(barcode);
        }

        // GET: Barcode/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Barcode/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Code")] Barcode barcode)
        {
            if (ModelState.IsValid)
            {
                _context.Add(barcode);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(barcode);
        }

        // GET: Barcode/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null || _context.Barcodes == null)
            {
                return NotFound();
            }

            var barcode = await _context.Barcodes.FindAsync(id);
            if (barcode == null)
            {
                return NotFound();
            }
            return View(barcode);
        }

        // POST: Barcode/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Code")] Barcode barcode)
        {
            if (id != barcode.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(barcode);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BarcodeExists(barcode.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(barcode);
        }

        // GET: Barcode/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null || _context.Barcodes == null)
            {
                return NotFound();
            }

            var barcode = await _context.Barcodes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (barcode == null)
            {
                return NotFound();
            }

            return View(barcode);
        }

        // POST: Barcode/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            if (_context.Barcodes == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Barcodes'  is null.");
            }
            var barcode = await _context.Barcodes.FindAsync(id);
            if (barcode != null)
            {
                _context.Barcodes.Remove(barcode);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BarcodeExists(string id)
        {
          return (_context.Barcodes?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
