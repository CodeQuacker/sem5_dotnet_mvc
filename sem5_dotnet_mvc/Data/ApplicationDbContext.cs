﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using sem5_dotnet_mvc.Models;

namespace sem5_dotnet_mvc.Data;

public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
{
    public DbSet<Barcode> Barcodes { get; set; } = null!;
    public DbSet<Category> Categories { get; set; } = null!;
    public DbSet<Item> Items { get; set; } = null!;
    public DbSet<ItemOrder> ItemOrders { get; set; } = null!;
    public DbSet<ItemProperty> ItemProperties { get; set; } = null!;
    public DbSet<Order> Orders { get; set; } = null!;
    public DbSet<Property> Properties { get; set; } = null!;
    public DbSet<Storage> Storages { get; set; } = null!;
    public DbSet<Ticket> Tickets { get; set; } = null!;
    public DbSet<Const> Consts { get; set; } = null!;
    public DbSet<Statistic> Statistics { get; set; } = null!;
    public DbSet<Mail> Mails { get; set; } = null!;
    

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }
}