﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class Barcode
{
    [Required]
    [Key]
    [Display(Name = "Barcode Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required(ErrorMessage = "Barcode code is required.")]
    [Display(Name = "Barcode code")]
    public string Code { get; set; } = null!;
}