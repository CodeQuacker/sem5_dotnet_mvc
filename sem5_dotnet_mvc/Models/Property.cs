﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class Property
{
    [Required]
    [Key]
    [Display(Name = "Property Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required(ErrorMessage = "Property name is required.")]
    [Display(Name = "Property name")]
    public string Name { get; set; } = null!;

    [Required(ErrorMessage = "Property value is required.")]
    [Display(Name = "Property value")]
    public string Value { get; set; } = null!;
}