﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class Category
{
    [Required]
    [Key]
    [Display(Name = "Category Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required(ErrorMessage = "Category name is required.")]
    [Display(Name = "Category name")]
    public string Name { get; set; } = null!;

    [Required(ErrorMessage = "Defining parent category is required. If this is top category set this value to null.")]
    [Display(Name = "Parent category Id")]
    public string? MasterCategoryId { get; set; }
    
    public virtual Category? MasterCategory { get; set; }
}