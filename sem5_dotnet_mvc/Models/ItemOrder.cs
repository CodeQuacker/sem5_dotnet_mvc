﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class ItemOrder
{
    [Required]
    [Key]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");

    [Required]
    public float Quantity { get; set; }

    [Required(ErrorMessage = "Order Id required")]
    public string OrderId { get; set; } = null!;
    public virtual Order Order { get; set; } = null!;
    
    [Required(ErrorMessage = "Item Id required")]
    public string Itemid { get; set; } = null!;
    public virtual Item Item { get; set; } = null!;
}