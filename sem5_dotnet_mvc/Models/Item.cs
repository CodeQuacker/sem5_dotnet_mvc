﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class Item
{
    [Required]
    [Key]
    [Display(Name = "Item Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required(ErrorMessage = "Item name is required.")]
    [MinLength(3, ErrorMessage = "Name is too short.")]
    [Display(Name = "Item name")]
    public string Name { get; set; } = null!;
    
    [Required(ErrorMessage = "Item value is required.")]
    [Display(Name = "Item value")]
    public float Value { get; set; }
    
    [Required(ErrorMessage = "Item amount is required.")]
    [Display(Name = "Item amount")]
    public float Amount { get; set; }
    
    [Required(ErrorMessage = "Item localization is required.")]
    [Display(Name = "Item Localization")]
    public string Localization { get; set; } = null!;
    
    [Display(Name = "Item barcode Id")]
    public string? BarcodeId { get; set; }
    public virtual Barcode? Barcode { get; set; }
    
    [Display(Name = "Item ticket Id")]
    public string? TicketId { get; set; }
    public virtual Ticket? Ticket { get; set; }
    
    [Required(ErrorMessage = "Item must have defined category.")]
    [Display(Name = "Item category Id")]
    public string? CategoryId { get; set; }
    public virtual Category? Category { get; set; }
    
    [Display(Name = "Item Storage Id")]
    public string? StorageId { get; set; }
    public virtual Storage? Storage { get; set; }
}