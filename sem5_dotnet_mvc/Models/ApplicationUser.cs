﻿using Microsoft.AspNetCore.Identity;

namespace sem5_dotnet_mvc.Models;

public class ApplicationUser : IdentityUser
{
    // TODO remove test property from account model
    // this is test property to be removet later, it does not represent anything,
    // it was created only to test if database created new attributes properly
    public string? Test { get; set; }
    
    // OLD ACCOUNT MODEL
    // [Required]
    // [Key]
    // [Display(Name = "Account Id")]
    // public string Id { get; set; } = Guid.NewGuid().ToString("N");
    //
    // [Required(ErrorMessage = "Account name is required.")]
    // [MinLength(3, ErrorMessage = "Name is too short.")]
    // [MaxLength(32, ErrorMessage = "Name is too long (max. 32 characters).")]
    // [Display(Name = "Account name")]
    // public string Name { get; set; } = null!;
    //
    // [Required(ErrorMessage = "Password is required.")]
    // [MinLength(8, ErrorMessage = "Password is too short (min. 8 characters).")]
    // [MaxLength(32, ErrorMessage = "Name is too long (max. 32 characters).")]
    // [Display(Name = "Password")]
    // public string Password { get; set; } = null!;
    //
    // [Display(Name = "Account permissions")]
    // public string? EntitlementId { get; set; }
    // public virtual Entitlement? Entitlement { get; set; }
    
}