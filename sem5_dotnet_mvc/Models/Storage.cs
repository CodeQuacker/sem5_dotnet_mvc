﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class Storage
{
    [Required]
    [Key]
    [Display(Name = "Storage Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required(ErrorMessage = "Storage name is required.")]
    [Display(Name = "Storage name")]
    public string Name { get; set; } = null!;
}