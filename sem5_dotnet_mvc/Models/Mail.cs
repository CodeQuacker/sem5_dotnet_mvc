﻿using System.ComponentModel.DataAnnotations;

namespace sem5_dotnet_mvc.Models;

public class Mail
{
    [Required]
    [Key]
    [Display(Name = "Mail Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    //TODO statistic type enum
    [Required(ErrorMessage = "Mail sender account id is required.")]
    [Display(Name = "Mail sender account id")]
    public string AccountIdFrom { get; set; } = null!;

    [Required(ErrorMessage = "Mail recipient account id is required.")]
    [Display(Name = "Mail recipient account id")]
    public string AccountIdTo { get; set; } = null!;
    
    [Required(ErrorMessage = "Mail message is required.")]
    [Display(Name = "Mail message")]
    public string Message { get; set; } = null!;
}