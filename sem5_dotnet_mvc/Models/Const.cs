﻿using System.ComponentModel.DataAnnotations;

namespace sem5_dotnet_mvc.Models;

public class Const
{
    [Required]
    [Key]
    [Display(Name = "Const Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    //TODO statistic type enum
    [Required(ErrorMessage = "Const name is required.")]
    [Display(Name = "Const name")]
    public string Name { get; set; } = null!;

    [Required(ErrorMessage = "Const value is required.")]
    [Display(Name = "Const value")]
    public string Value { get; set; } = null!;
}