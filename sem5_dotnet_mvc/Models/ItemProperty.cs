﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class ItemProperty
{
    [Required]
    [Key]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required(ErrorMessage = "Property Id required")]
    public string PropertyId { get; set; } = null!;
    public virtual Property Property { get; set; } = null!;
    
    [Required(ErrorMessage = "Item Id required")]
    public string Itemid { get; set; } = null!;
    public virtual Item Item { get; set; } = null!;
}