﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class Order
{
    [Required]
    [Key]
    [Display(Name = "Order Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    //TODO change state type to enum
    [Required(ErrorMessage = "Order state is required.")]
    [Display(Name = "Order state")]
    public string State { get; set; } = null!;
    
    [Required(ErrorMessage = "Ordering account id is required.")]
    [Display(Name = "Ordering account Id")]
    public string AccountId { get; set; } = null!;
    public virtual ApplicationUser ApplicationUser { get; set; } = null!;
}