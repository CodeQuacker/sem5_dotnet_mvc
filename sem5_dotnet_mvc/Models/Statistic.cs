﻿using System.ComponentModel.DataAnnotations;

namespace sem5_dotnet_mvc.Models;

public class Statistic
{
    [Required]
    [Key]
    [Display(Name = "Statistic Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    //TODO statistic type enum
    [Required(ErrorMessage = "Statistic type is required.")]
    [Display(Name = "Statistic type")]
    public string Name { get; set; } = null!;

    [Required(ErrorMessage = "Statistic value is required.")]
    [Display(Name = "Statistic timestamp")]
    public DataType Date { get; set; }
    
    [Required(ErrorMessage = "Statistic value is required.")]
    [Display(Name = "Statistic value")]
    public string Value { get; set; } = null!;
}