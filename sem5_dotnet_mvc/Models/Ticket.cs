﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace sem5_dotnet_mvc.Models;

public class Ticket
{
    [Required]
    [Key]
    [Display(Name = "Ticket Id")]
    public string Id { get; set; } = Guid.NewGuid().ToString("N");
    
    [Required(ErrorMessage = "Ticket file path is required.")]
    [Display(Name = "File path")]
    public string TicketFile { get; set; } = null!;
}